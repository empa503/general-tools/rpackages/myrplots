% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/multi.panel.grids.R
\name{multi.panel.grids}
\alias{multi.panel.grids}
\title{Creates a multi panel plot for objects of class 'reg.grid' and 'irr.grid'}
\usage{
multi.panel.grids(
  grids,
  nn.col,
  nn.row,
  gap = 0,
  mar = c(4, 4, 1, 1) + 0.1,
  by.row = TRUE,
  axis.label.all = FALSE,
  xlab = NULL,
  ylab = NULL,
  zlim = NULL,
  panel.label = FALSE,
  panel.label.line = -1,
  panel.label.cex = 1,
  panel.title = NULL,
  panel.title.pos = "bottom",
  key.pos = 4,
  key.width = 0.6,
  panel.FUN = NULL,
  ...
)
}
\arguments{
\item{grids}{list of objects of type 'reg.grid' or 'irr.grid'}

\item{nn.col}{optional parameter giving the number of columns in the panel grid}

\item{nn.row}{optional parameter giving the number of rows in the panel grid}

\item{gap}{gap between inner panels given as number of lines.}

\item{mar}{plot margins given as number of lines on each side of plot. See \code{\link{par}}}

\item{by.row}{Switch for plotting order of panels. Default is first by row, by.row=TRUE}

\item{axis.label.all}{If FALSE (default) only the left and lower panels will receive axis labels
and titles.}

\item{xlab}{Title for x-axis.}

\item{ylab}{Title for y-axis.}

\item{zlim}{Value range for color key. If not provided or NULL range will be taken from the 
range of the first element of 'grids'.}

\item{panel.label}{If TRUE label panels by lower case letters.}

\item{panel.label.line}{Line on which to draw the panel label. Default is -1, which means 
inside the plot region. Panel labels will be drawn on the upper left side 
of each panel.}

\item{panel.label.cex}{Character expansion factor. Default is 1. Same size as axis labels.}

\item{panel.title}{Vector of character titles for each panel. Omit individual labels by
interspersing with NA. The titles are either plotted on the same line as 
the labels (right adjusted) or at the location specified by 'panel.title.pos'. 
If 'panel.title.pos' is not NA title will be plotted into a semi-transparent
textbox (see 'textbox') else as a simple text.}

\item{panel.title.pos}{Character string giving the position of the 'panel.title'. Possible
values: 'top', 'topright', 'topleft', 'left', 'right', 'bottom', 'bottomright',
'bottomleft'.}

\item{key.width}{Width of color key in inches. Value might lead to 'figure margins too small'
error. In which case it should be increased.}

\item{panel.FUN}{Function that gets executed after every individual sub-panel plot. Can 
be used to add further elements to each sub-panel. Note that no arguments are
passed to the function.}

\item{...}{additional arguments passed to 'fill.2dplot' and/or 'plot.irr.grid'}
}
\value{
Nothing
}
\description{
The main idea of the multi panel plot is to avoid repeated drawing of axis labels and titles
for each sub-panel. Hence a common xlim and ylim needs to be assured by the user. However,
using an appropriate value for 'gap' and force plotting of all axis labels and titles 
'axis.label.all=TRUE' the multi panel concept will also work with different xlim and ylim 
ranges.
}
\examples{
		reg.grid = list(xrng=c(0,20), yrng=c(35,50), zz = matrix(rnorm(100), 10, 10))
	class(reg.grid) = "reg.grid"

		multi.panel.grids(rep(list(reg.grid),4), by.row=TRUE, zlim=c(-2,2), xlab="Long", 
	key.title="asdf", mar=c(3,3,1,1)+.1, gap=5, axis.label.all=FALSE, panel.label=TRUE, 
	panel.label.line=-1.5, panel.title="test", panel.label.cex=1.5, plot.grid=TRUE)


}
