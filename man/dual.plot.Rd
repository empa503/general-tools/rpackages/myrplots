\name{dual.plot}
\alias{dual.plot}
\title{ Line plot with two y-axes }
\description{
    Creates a plot of two vectors versus two different y-axes and a common x-axis. It is possible to include error bars.
}
\usage{
dual.plot(xx, yy1, yy2, ylab1, ylab2, u.yy1, u.yy2, xlab, type = "l", 
        col = c(1, 1), lty = c(1, 1), at, labels, xlim, ylim1, ylim2, axis.FUN=axis, ...)
}
\arguments{
  \item{xx}{ (numeric) vector; x-values }
  \item{yy1}{ (numeric) vector; y1-values  }
  \item{yy2}{ (numeric) vector; y2-values }
  \item{ylab1}{ (character) label for first y-axis }
  \item{ylab2}{ (character) label for second y-axis }
  \item{u.yy1}{ (numeric) vector; uncertainties for y1-values }
  \item{u.yy2}{ (numeric) vector; uncertainties for y2-values }
  \item{xlab}{ (character) x-axis label }
  \item{type}{ (character) type of plot: "l", "p" }
  \item{col}{ 2 element color specification for left and right y-axis and connected lines }
  \item{lty}{ (numeric) 2 element vector of line type }
  \item{at}{ (numeric) where to plot x-axis labels }
  \item{labels}{ (character) x-axis labels }
  \item{xlim}{ (numeric) 2 element vector, limits of x-axis }
  \item{ylim1}{ (numeric) 2 element vector, limits of left y-axis }  
  \item{ylim2}{ (numeric) 2 element vector, limits of right y-axis }  
  \item{axis.FUN}{ (function) function used for x-axis plot, "axis" is default }
  \item{\dots}{ other parameters passed to 'points' or 'lines' }
}
\author{ stephan.henne@empa.ch }
\examples{
    graphics.off()
    xx = 1:12
    yy1 = rnorm(12)
    yy2 = rnorm(12, 4)^2
    uyy1 = abs(rnorm(12))
    uyy2 = 1:12
    dual.plot(xx, yy1, yy2, u.yy1=uyy1, u.yy2=uyy2, xlab="Month", col=c("blue", "gold"), at=1:12, labels=month.abb)
}
\keyword{ hplot }

