#' R color to hex notation
#'
#' Returns hex color string for color defined by name. 
#' Transparency (alpha) can be added as well. 
#' 
#' @param vector of any of the R color specifications: either a color name 
#' 		(as listed by 'colors()') or a positive integer referring to 'palette()[i]'.
#' @param alpha alpha level to be added to color. Valid values between 0 and 255.
#'
#' @return A hexadecimal string for each color in the form '#rrggbb' or '#rrggbbbaa'.
#'
#' @seealso  \code{\link{col2rgb}}
#' 
#' @export
col2hex = function(col, alpha){
	
	col = col2rgb(col)
	col = rgb(t(col), maxColorValue=255, alpha=alpha)

	return(col)
}

#print(col2hex("green"))

#print(col2hex(c(palette=1:3), alpha=c(100,200,130)))
