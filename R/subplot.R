#' Insert a sub-plot into the plot area of the currently active plot.
#' 
#' Inserts a sub-plot into the plot area of the currently active plot. The requirements on the
#' plot function used in the sub-plot are described below. 
#'
#' @param fun An expression or function defining the new sub-plot. It is crucial that this plot
#'		does not call plot.new or changes the graphical parameters of the current device. 
#' 		Otherwise the outcome might be unpredictable especially when using subplot together 
#'		\code{\link{layout}}.
#' @param x (numeric) x-coordinates of the subplot in user coordinates of the current plot
#' @param y (numeric) y-coordinates of the subplot in user coordinates of the current plot
#' @param size (numeric) The size of the subplot in inces if x and y contain only one element.
#' @param vadj (numeric) Vertical adjustment of the subplot when y only contains one element.
#' 			The subplot will be aligned relative to y. 1 top of subplot at y. 0 bottom of 
#'			subplot at y.
#' @param hadj (numeric) Horizontal adjustment of the subplot when x only contains one element.
#' 			The subplot will be aligned relative to x. 1 right edge of subplot at x. 0 left edge of 
#'			subplot at x.
#' @param pars List of graphical parameters passed to \code{\link{par}} before running plot.
#' @param bg A color that will be used to plot a rectangle in the same color behind the
#'			plot area of the new subplot.
#'  
#' @return list of device parameters 
#'
#' @author	original function taken from Hmisc package by Frank E Harrell Jr 
#' 
#' @examples
#' 
#' # map barplot in lower left corner 
#' my.map(xlim=c(-10,40), ylim=c(35, 60))
#' box()
#' axis(1)
#' axis(2)
#' subplot(barplot(c(1,2), xaxt="n", yaxt="n", xlab="", ylab=""), x=c(20,40), 
#'   y=c(35,45), bg=col2hex("gray", alpha=128))
#'
#' @export 
subplot <- function (fun, x, y = NULL, size = c(1, 1), vadj = 0.5,
                     hadj = 0.5, pars = NULL, bg, mar=c(4,4,1,1)+.1) {

	old.par <- par(no.readonly = TRUE, "plt", "usr")
	on.exit({par(old.par); clip(par("usr")[1], par("usr")[2], par("usr")[3], par("usr")[4])})
	if (missing(x))
		x <- locator(2)

	xy <- xy.coords(x, y)

	#	plot a rectangle first to fill background
	if (!missing(bg)){
#		rect(xy$x[1], xy$y[1], xy$x[2], xy$y[2], col=bg, border=FALSE)

		#	xy are in usr coordinates and give the plot region of subplot (no margins included)
		#	expand with margins
		dudx = diff(par("usr")[1:2])/par("pin")[1]
		dudy = diff(par("usr")[3:4])/par("pin")[2]
		
		mar = mar * par("cin")[2]
		mar[1] = mar[1] * dudy
		mar[2] = mar[2] * dudx
		mar[3] = mar[3] * dudy
		mar[4] = mar[4] * dudx
		
		rect(x[1]-mar[2], y[1]-mar[1], x[2]+mar[4], y[2]+mar[3], col=bg, border=FALSE)
	}

  	if (length(xy$x) != 2) {
		pin <- par("pin")
		tmp <- cnvrt.coords(xy$x[1], xy$y[1], "usr")$plt
		x <- c(tmp$x - hadj * size[1]/pin[1], tmp$x + (1 - hadj) *
           size[1]/pin[1])
		y <- c(tmp$y - vadj * size[2]/pin[2], tmp$y + (1 - vadj) *
           size[2]/pin[2])
		xy <- cnvrt.coords(x, y, "plt")$fig
	} else {
    	xy <- cnvrt.coords(xy, , "usr")$fig
	}

	if (length(pars)) par(pars)

	par(plt = c(xy$x, xy$y), new = TRUE)
	if (is.function(fun)) fun() else fun
	tmp.par <- par(no.readonly = TRUE)

	return(invisible(tmp.par))
}


##	EXAMPLES
#
##	map overlay with semi-transparent background
#require(myRplots)
#
#my.map(xlim=c(-10,40), ylim=c(35, 60))
#box()
#axis(1)
#axis(2)
#subplot(barplot(c(1,2), xaxt="n", yaxt="n", xlab="", ylab=""), x=c(20,40), y=c(35,45), bg=col2hex("gray", alpha=128))
