#	plot histogram of data and a fitted normal distribution

hist.norm = function(x, na.rm=FALSE, line.col=2, freq=TRUE, probability= !freq, mn.x, sig.x, ...){
	if (!missing(probability)) freq = !probability
	if (missing(sig.x)) {
		sig.x = sd(x, na.rm=na.rm)
	}
	if (missing(mn.x)) {
		mn.x = mean(x, na.rm=na.rm)
	}

	nn = length(which(!is.na(x)))
	hres = hist(x, freq=freq, probability=probability, ...)

	xx = seq(min(hres$breaks), max(hres$breaks), length.out=200)
	dx = diff(hres$breaks[1:2])
	yy = 1/sqrt(2*pi)/sig.x*exp(-(xx-mn.x)^2/(2*sig.x^2))
	if (freq && hres$equidist) yy = yy*nn*dx
	lines(xx,yy,col=line.col)
	mtext(paste("mean:", signif(mn.x,3)), 3, -1, adj=0.95, col=line.col)
	mtext(paste("sd:", signif(sig.x,3)), 3, -2, adj=0.95, col=line.col)

	return(invisible(hres))
}
